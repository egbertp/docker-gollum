# Golum wiki as Docker image

```sh
$ docker build -t gollum:latest .
```

## Start gollum

```sh
$ cd ~/Development/knowledge-base
$ docker run -v `pwd`:/wiki -p 4567:80 gollum
```
